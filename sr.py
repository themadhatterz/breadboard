import sys
import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)

CLOCK = 17 # CLOCK / SRCLK
LATCH = 27 # LATCH / RCLK
DATA = 22 # DATA / SER

GPIO.setup(CLOCK, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LATCH, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(DATA, GPIO.OUT, initial=GPIO.LOW)

def shiftout(byte):
    GPIO.output(LATCH, GPIO.LOW)
    for x in range(8):
        GPIO.output(DATA, (byte >> x) & 1)
        GPIO.output(CLOCK, 1)
        GPIO.output(CLOCK, 0)
    GPIO.output(LATCH, GPIO.HIGH)

try:
    while True:
        # count up
        # for x in range(0, 255):
        #     shiftout(x)
        #     time.sleep(0.05)
        # count down
        # for x in reversed(range(0, 255)):
        #     shiftout(x)
        #     time.sleep(0.05)
        # row flash
        # for x in [1, 3, 7, 15, 31, 63, 127, 254, 252, 240, 224, 192, 128, 0]:
        #     shiftout(x)
        #     time.sleep(0.1)
        # 3 row flash
        # for x in [1, 3, 7, 14, 28, 56, 112, 224, 192, 128, 0]:
        #     shiftout(x)
        #     time.sleep(0.05)
        # pong
        for x in [1, 1, 1, 2, 4, 8, 16, 32, 64, 128, 128, 128, 64, 32, 16, 8, 4, 2]:
            shiftout(x)
            time.sleep(0.05)
except KeyboardInterrupt:
    pass

GPIO.cleanup()