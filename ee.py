import sys
import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)

CLOCK = 17 # CLOCK / SRCLK
LATCH = 27 # LATCH / RCLK
DATA = 22 # DATA / SER

GPIO.setup(CLOCK, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LATCH, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(DATA, GPIO.OUT, initial=GPIO.LOW)

def shiftout(byte):
    GPIO.output(LATCH, GPIO.LOW)
    for x in range(8):
        GPIO.output(DATA, (byte >> x) & 1)
        GPIO.output(CLOCK, 1)
        GPIO.output(CLOCK, 0)
    GPIO.output(LATCH, GPIO.HIGH)

try:
    while True:
        for x in range(0, 255):
            shiftout(x)
            time.sleep(0.05)
except KeyboardInterrupt:
    pass

GPIO.cleanup()