import time

import RPi.GPIO as GPIO

RED = 17
YLW = 27
GRN = 22


if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    # GPIO.setwarnings(False)

    GPIO.setup(RED, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(YLW, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(GRN, GPIO.OUT, initial=GPIO.LOW)

    lights = [
        {"color": RED, "length": 30},
        {"color": GRN, "length": 30},
        {"color": YLW, "length": 3},
    ]

    try:
        while True:
            for light in lights:
                GPIO.output(light["color"], GPIO.HIGH)
                time.sleep(light["length"])
                GPIO.output(light["color"], GPIO.LOW)
    except KeyboardInterrupt:
        pass

    GPIO.cleanup()