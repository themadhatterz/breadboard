import sys
import time

import RPi.GPIO as GPIO
import requests

def button_callback(pin):
    # print(pin)
    print("Button pushed!")
    r = requests.get("http://192.168.1.204:8888/", timeout=1)
    print(r)
    time.sleep(0.1)



def main():
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    # GPIO.setwarnings(False)
    GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(4, GPIO.RISING, callback=button_callback)

    main()

    GPIO.cleanup()