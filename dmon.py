import sys
import time

import asyncio
import discord
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)

GUS_PIN = 5 # WHITE
KYLE_PIN = 6 # GREEN
KORY_PIN = 13 # RED
BILLY_PIN = 16 # BLUE
CHRIS_PIN = 26 # YELLOW
# HATTER_PIN = 26 # YELLOW
SOUND_PIN = 12 # Active Buzzer


GPIO.setup(GUS_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(KYLE_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(KORY_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(BILLY_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(CHRIS_PIN, GPIO.OUT, initial=GPIO.LOW)
# GPIO.setup(HATTER_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(SOUND_PIN, GPIO.OUT, initial=GPIO.LOW)


client_token = "NzYyNDExOTQyMDkzNzgzMDQz.X3oxcw.HG7yvP-XE1M-rw00uiQtLFar0qk"
intents = discord.Intents.default()
intents.members = True
intents.presences = True
client = discord.Client(intents=intents)


async def beep(times):
    for _ in range(times):
        GPIO.output(SOUND_PIN, GPIO.HIGH)
        await asyncio.sleep(0.2)
        GPIO.output(SOUND_PIN, GPIO.LOW)
        await asyncio.sleep(0.2)


async def member_status():
    members = [
        {"name": "Caesar", "pin": GUS_PIN, "state": discord.Status.offline},
        {"name": "Kyle", "pin": KYLE_PIN, "state": discord.Status.offline},
        {"name": "holste", "pin": BILLY_PIN, "state": discord.Status.offline},
        {"name": "Space Kook Kory", "pin": KORY_PIN, "state": discord.Status.offline},
        {"name": "buddyaces", "pin": CHRIS_PIN, "state": discord.Status.offline},
        # {"name": "hatterz", "pin": HATTER_PIN, "state": discord.Status.offline},
    ]
    while True:
        await client.wait_until_ready()
        guild = client.get_guild(760987901483024387)
        for member in members:
            member_object = await guild.query_members(member["name"], presences=True)
            newstatus = member_object[0].status
            if newstatus != member["state"]:
                member["state"] = newstatus
                if newstatus == discord.Status.online:
                    await beep(2)
                    GPIO.output(member["pin"], GPIO.HIGH)
                else:
                    await beep(1)
                    GPIO.output(member["pin"], GPIO.LOW)

        await asyncio.sleep(3)


client.loop.create_task(member_status())
client.run(client_token)


GPIO.cleanup()