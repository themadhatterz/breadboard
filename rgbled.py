import sys
import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)

GPIO.setup(4, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(27, GPIO.OUT, initial=GPIO.LOW)
sleep_interval = 0.5

def red(state=GPIO.LOW):
    GPIO.output(4, state)


def green(state=GPIO.LOW):
    GPIO.output(17, state)


def blue(state=GPIO.LOW):
    GPIO.output(27, state)


while True:
    try:
        red(GPIO.HIGH)
        time.sleep(sleep_interval)
        blue(GPIO.HIGH)
        time.sleep(sleep_interval)
        red()
        time.sleep(sleep_interval)
        green(GPIO.HIGH)
        time.sleep(sleep_interval)
        blue()
        time.sleep(sleep_interval)
        red(GPIO.HIGH)
        time.sleep(sleep_interval)
        green()
    except KeyboardInterrupt as e:
        break

GPIO.cleanup()