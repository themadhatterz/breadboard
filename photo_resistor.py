import sys
import time

import RPi.GPIO as GPIO


def main(pin):
    count = 0

    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.1)
    GPIO.setup(pin, GPIO.IN)

    while GPIO.input(pin) == GPIO.LOW:
        count += 1
    
    return count



if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    # GPIO.setwarnings(False)

    GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)

    while True:
        try:
            level = main(4)
            if level >= 40000:
                GPIO.output(17, GPIO.HIGH)
            else:
                GPIO.output(17, GPIO.LOW)
        except KeyboardInterrupt:
            break

    GPIO.cleanup()